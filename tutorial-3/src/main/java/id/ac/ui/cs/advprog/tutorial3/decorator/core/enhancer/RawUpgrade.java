package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.Randomizer;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    int upgradedValue;

    public RawUpgrade(Weapon weapon) {
        upgradedValue = Randomizer.randInt(5,10);
        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return "Raw "+weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        return upgradedValue+(weapon != null ? weapon.getWeaponValue() : 0);
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Raw "+weapon.getDescription();
    }
}
