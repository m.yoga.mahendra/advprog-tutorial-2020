package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.Randomizer;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    int upgradedValue;

    public MagicUpgrade(Weapon weapon) {
        upgradedValue = Randomizer.randInt(15,20);
        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return "Magic "+weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        return upgradedValue + (weapon != null ? weapon.getWeaponValue() : 0);
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Magic "+weapon.getDescription();
    }
}
