package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.Randomizer;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;
    int upgradedValue;

    public UniqueUpgrade(Weapon weapon){
        upgradedValue = Randomizer.randInt(10,15);
        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return "Unique "+weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        return upgradedValue + (weapon != null ? weapon.getWeaponValue() : 0);
    }

    @Override
    public String getDescription() {
        return "Unique "+weapon.getDescription();
        //TODO: Complete me
    }
}
