package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class GunTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Gun();
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Gun",weapon.getName());
        //TODO: Complete me
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals("Automatic Gun",weapon.getDescription());
        //TODO: Complete me
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertEquals(20,weapon.getWeaponValue());
    }
}
