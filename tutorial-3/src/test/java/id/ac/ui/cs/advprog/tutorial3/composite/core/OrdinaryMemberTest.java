package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Asuna", "The Flash");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        assertEquals("Asuna",member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("The Flash",member.getRole());
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        //TODO: Complete me
        Member tempChild = new OrdinaryMember("asdasd","asdasdsa");
        member.addChildMember(tempChild);
        assertEquals(0,member.getChildMembers().size());
        member.removeChildMember(tempChild);
        assertEquals(0,member.getChildMembers().size());
    }
}
