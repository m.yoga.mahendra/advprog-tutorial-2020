package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Bujang", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        guild.addMember(guildMaster,new OrdinaryMember("Test","JuniorMaster"));
        assertEquals(2,guild.getMemberList().size());
    }
    @Test
    public void testMethodCannotAddMemberAsMaster() {
        guild.addMember(guildMaster,new OrdinaryMember("Test","Master"));
        assertEquals(1,guild.getMemberList().size());
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        guild.addMember(guildMaster,new OrdinaryMember("Test","JuniorMaster"));
        guild.removeMember(guildMaster,guild.getMember("Test","JuniorMaster"));
        assertEquals(1,guild.getMemberList().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member bambang = new OrdinaryMember("Bambang", "Pamungkas");
        guild.addMember(guildMaster, bambang);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(bambang, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member child = new OrdinaryMember("Test","JuniorMaster");
        guild.addMember(guildMaster,child);
        assertEquals(child,guild.getMember("Test","JuniorMaster"));

        //TODO: Complete me
    }
}
