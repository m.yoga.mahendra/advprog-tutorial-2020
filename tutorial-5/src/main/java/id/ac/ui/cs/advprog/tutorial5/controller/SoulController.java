package id.ac.ui.cs.advprog.tutorial5.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.services.SoulService;

@RestController
@RequestMapping(path = "/soul")
public class SoulController {

	@Autowired
    private final SoulService soulService;
    
    public SoulController(SoulService soulService) {
        this.soulService = soulService;
    }

    @GetMapping
    public ResponseEntity<List<Soul>> findAll() {
        return ResponseEntity.ok(soulService.findAll());
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Soul soul) {
        return ResponseEntity.ok(soulService.register(soul));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Soul> findById(@PathVariable Long id) {
        return ResponseEntity.ok(soulService.findSoul(id).get());
    }
	
	@PutMapping("/{id}")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
        if (id == soul.getId())
            return ResponseEntity.ok(soulService.rewrite(soul));
        return ResponseEntity.ok(soul);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        soulService.erase(id);
        return ResponseEntity.ok("deleted");
    }
}
