package id.ac.ui.cs.advprog.tutorial5.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import id.ac.ui.cs.advprog.tutorial5.services.SoulServiceImpl;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImplTest {
    @Mock
    private SoulRepository soulRepository;

    @InjectMocks
    private SoulServiceImpl soulService;

    @Test
    public void whenFindAllIsCalledItShouldCallSoulRepositoryFindAll() {
        soulService.findAll();

        verify(soulRepository, times(1)).findAll();
    }

    @Test
    public void whenFindSoulIsCalledItShouldCallSoulRepositoryFindById() {
        soulService.findSoul((long) 1);

        verify(soulRepository, times(1)).findById((long) 1);
    }

    @Test
    public void whenEraseIsCalledItShouldCallSoulRepositoryDeleteById() {
        soulService.erase((long) 1);

        verify(soulRepository, times(1)).deleteById((long) 1);
    }

    @Test
    public void whenRewriteIsCalledItShouldCallSoulRepositorySave() {
        Soul soul = new Soul();

        soulService.rewrite(soul);

        verify(soulRepository, times(1)).save(soul);
    }

    @Test
    public void whenRegisterIsCalledItShouldCallSoulRepositorySave() {
        Soul soul = new Soul();

        soulService.register(soul);

        verify(soulRepository, times(1)).save(soul);
    }
    @Test
    public void testRegisterSoulAndDeleteSoul(){
        Soul soul = new Soul();
        soulService.register(soul);
        assertNotNull(soul.getId());
        soulService.erase(soul.getId());
        assertEquals(soulService.findSoul(soul.getId()),Optional.empty());
    }
}
