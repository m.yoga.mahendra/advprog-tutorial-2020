package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private static HolyWish singleton = new HolyWish();

    private String wish;

    private HolyWish(){
        wish="";
    }

    public static HolyWish getInstance() {return singleton;}

    // TODO complete me with any Singleton approach

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}