package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.skill;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ThousandYearsOfPainTest {

    Skill thousandYearsOfPain;

    @BeforeEach
    public void setUp(){
        thousandYearsOfPain = new ThousandYearsOfPain();
    }

    @Test
    public void testToString(){
       // TODO create test
        assertEquals("Thousand Years Of Pain, Force that causes so much Pain",thousandYearsOfPain.toString());
    }
    @Test
    public void testGetName(){
        // TODO create test
        assertEquals("Thousand Years Of Pain",thousandYearsOfPain.getName());
    }
    @Test
    public void testDescription(){
        // TODO create test
        assertEquals("Force that causes so much Pain",thousandYearsOfPain.getDescription());
    }
}
