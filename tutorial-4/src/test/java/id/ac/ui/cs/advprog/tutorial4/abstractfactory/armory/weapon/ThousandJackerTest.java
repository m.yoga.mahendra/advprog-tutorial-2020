package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.weapon;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ThousandJackerTest {

    Weapon thousandJacker;

    @BeforeEach
    public void setUp(){
        thousandJacker = new ThousandJacker();
    }

    @Test
    public void testToString(){
        // TODO create test
        assertEquals("Thousand Jacker, Attack with a thousand Jacker",thousandJacker.toString());
    }
    @Test
    public void testGetName(){
        assertEquals("Thousand Jacker",thousandJacker.getName());
        // TODO create test
    }
    @Test
    public void testDescription(){
        assertEquals("Attack with a thousand Jacker",thousandJacker.getDescription());
        // TODO create test
    }
}
